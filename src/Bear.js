import React, { Component } from 'react'
import axios from 'axios'
import './App.css'

const URL = 'http://gtfarng-restful.ddns.net:8004/api/';

export default class Bear extends Component {
    constructor(props) {
        super(props)
        this.state = {
            bears: [],
            id: 0, name: '', weight: 0,
            newId: 0, newName: '', newWeight: 0,
        }
    }

    componentDidMount() {
        this.getAllBears()
    }

    getAllBears() {
        axios.get(`${URL}/bears`)
            .then(res => {
                this.setState({ bears: res.data })
                console.log(res.data)
            }
            )
            .catch((error) => { console.log(error); })
    }

    renderBears() {
        return this.state.bears.map((bear, index) => {
            if (bear !== null)
                return (
                <h5>
                        <li  className="bullet" key={bear.id} >
                             {bear.id}. Name : <small>{bear.name} </small>, Weight : <small> {bear.weight}  </small> Kg. &nbsp;&nbsp;
                            <button class="btn btn-primary" onClick={() => this.getBear(index)}>Get</button>  &nbsp;
                            <button class="btn btn-danger" onClick={() => this.deleteBear(index)}>Delete</button><br /><br />
                            
                        </li>
                        </h5>
                )
            return ('')
        })
    }

    deleteBear(id) {
        axios.delete(`${URL}/bear/${id}`)
            .then((res) => {
                console.log('Delete:' + res)
                this.getAllBears()  // Get all bear
            })
    }

    addBear = (e) => {
        e.preventDefault()
        axios.post(`${URL}/bears`, {
            name: this.state.name,
            weight: this.state.weight
        })
            .then((res) => {
                console.log('Create a new bear: ' + res);
                this.getAllBears()  // Get all bear
            })
    }

    getBear(id) {
        axios.get(`${URL}/bear/${id}`)
            .then((res) => {
                const { name, weight } = res.data
                console.log(res.data)
                this.setState({ newId: id, newName: name, newWeight: weight })
                console.log(this.state)
            })
    }

    editBear = (e) => {
        e.preventDefault()
        axios.put(`${URL}/bear/${this.state.newId}`, {
            name: this.state.newName,
            weight: this.state.newWeight
        })
            .then((response) => {
                console.log('Create a new bear: ' + response);
                this.getAllBears()  // Get all bear
            })
    }

    handleChange = (e) => {
        const { name, value } = e.target
        this.setState({ [name]: value })
    }

    render() {
        return (
            <div>
                <br />
                <div>
                    <h2 className="head"> Bear Profile</h2><br />
                    <ul>
                        {this.renderBears()}
                    </ul>

                </div>

                <br /> <br />
                <div>
                    <h2 className="head">Add Bear</h2><br />
                    <form onSubmit={this.addBear}>
                        <input type="text" name="name"
                            onChange={this.handleChange}
                            placeholder="Name..." />&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="number" name="weight"
                            onChange={this.handleChange}
                            placeholder="Weight..." />
                        <br /> <br />
                        <button class="btn btn-success">Submit</button>
                    </form>
                </div>

                <br /> <br />
                <div>
                    <h2 className="head">Edit Bear</h2><br />
                    <form onSubmit={this.editBear}>
                        <input type="text" name="newName"
                            value={this.state.newName}
                            onChange={this.handleChange} />&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="number" name="newWeight"
                            value={this.state.newWeight}
                            onChange={this.handleChange} />
                        <input type="hidden" name="newId"
                            value={this.state.newId}
                            onChange={this.handleChange} />
                        <br /> <br />
                        <button class="btn btn-success">Submit</button>
                    </form>

                </div>
                <br /> <br /> <br /> <br />
            </div>
        );
    }

}

