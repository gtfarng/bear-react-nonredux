import React, { Component } from 'react'
import './App.css';
import Bear from './Bear'

export default class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <br /><h1>My-ReactApp</h1><br />
        </header>
        <div>
          <Bear />
        </div>

      </div>
    );
  }
}


